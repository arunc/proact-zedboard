#!/usr/bin/env bash


copy_run () {
    sshpass -p 'root' scp ./programs/${1}-approxDisabled.riscv root@192.168.1.5:~/    
    sshpass -p 'root' ssh root@192.168.1.5 -t "./fesvr-zedboard  ${1}-approxDisabled.riscv > ${1}-approxDisabled.rpt "
    sshpass -p 'root' scp root@192.168.1.5:~/${1}-approxDisabled.rpt   ./


    sshpass -p 'root' scp ./programs/${1}-approx20.riscv root@192.168.1.5:~/    
    sshpass -p 'root' ssh root@192.168.1.5 -t "./fesvr-zedboard  ${1}-approx20.riscv > ${1}-approx20.rpt "
    sshpass -p 'root' scp root@192.168.1.5:~/${1}-approx20.rpt   ./

}

command -v sshpass >/dev/null 2>&1 || { echo >&2 "Program sshpass is not installed. Either install sshpass or copy all these files manually.  Aborting."; exit 1; }
command -v scp >/dev/null 2>&1 || { echo >&2 "Program scp is not installed. Must have scp and ssh to communicate with zedboard.  Aborting."; exit 1; }


copy_run sinhx
copy_run coshx
copy_run tanhx

copy_run asinhx
copy_run acoshx
copy_run atanhx
