## PROACT-ZEDBOARD : Zedboard porting for Proact processor.

Build and deploy Proact processor boot images in Zedboard.

And further run some standard C-programs in the Zedboard using Proact.

# Video for build steps : 

# Steps summary:

1) make project   # Will create a Xilinx Vivado Project 

2) make zedboard  # Generates the bitstream, boot.bin etc for this project.

3) make load-sd SD=/path/to/sd-card  # Copies the generated images to Zedboard SD card.

Put the SD card into the Zedboard, power-up, connect it to through ethernet.

Ping the zedboard (192.168.1.5) to make sure it is responding.

After this prepare-zedboard with the necessary binaries to interact with Proact

4) make prepare-zedboard


Finally login into Zedboard and do a basic "hello" testing.

ssh root@192.168.1.5   # password and username both root

root@zynq:~#./fesvr-zedboard pk ./hello
hello!



# Notes:

1) Vivado 16.2 version used. I haven't checked for the other versions.

2) All the input verilog sources and other zedboard files should be available.

3) Creating verilog sources for proact is maintained as another project  : git@gitlab.com:arunc/proact-processor.git 


################################################################################

################################################################################


To modify/build sources (Verilog, Chisel etc)

make proact


Notes : 

1) The project directory must be a subdirectory of proact-processor project 

(git@gitlab.com:arunc/proact-processor.git) inside proact-processor/build/

2) All the GCC tools should be built for proact-processor before running this Makeflow

#-----------------------------------------------------------------------------------------------------------
